from django.test import TestCase
from django.core import mail
from django.test.client import Client
from django.core.urlresolvers import reverse
from django.conf import settings

from model_mommy import mommy

from simplemooc.courses.models import Course


class CourseManagerTestCase(TestCase):

    def setUp(self):
        self.courses_django = mommy.make(
            'courses.Course', name='Python na Web com Django',
            _quantity=5
        )
        self.course_dev = mommy.make(
            'courses.Course', name='Python para Devs',
            _quantity=10
        )
        self.client = Client()

    def tearDown(self):
        # for course in self.courses:
        #    course.delete()
        Course.objects.all().delete()

    def teste_course_search(self):
        search = Course.objects.search('django')
        self.assertEqual(len(search), 5)
        search = Course.objects.search('devs')
        self.assertEqual(len(search), 10)
        search = Course.objects.search('Python')
        self.assertEqual(len(search), 15)
